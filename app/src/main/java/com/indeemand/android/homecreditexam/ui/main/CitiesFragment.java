package com.indeemand.android.homecreditexam.ui.main;

import android.arch.lifecycle.Observer;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.google.gson.Gson;
import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.base.BaseFragment;
import com.indeemand.android.homecreditexam.base.Constants;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.indeemand.android.homecreditexam.ui.detail.DetailActivity;

import java.util.List;

import io.reactivex.disposables.Disposable;

public class CitiesFragment extends BaseFragment<CitiesFragmentViewModel> {

    private CitiesRecyclerViewAdapter citiesAdapter;
    private RecyclerView recCities;

    @Override
    protected String getPageName() {
        return "Cities List View";
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_list;
    }

    @Override
    protected Class<CitiesFragmentViewModel> getViewModelClass() {
        return CitiesFragmentViewModel.class;
    }

    @Override
    protected void setupViews(@NonNull View view) {
        citiesAdapter = new CitiesRecyclerViewAdapter();

        recCities = view.findViewById(R.id.recCities);
        recCities.setAdapter(citiesAdapter);

    }

    @Override
    protected void setupInputs(@NonNull View view) {
        Disposable itemClickedDisposable = citiesAdapter.itemClicked.subscribe(currentWeatherItem -> {
            Intent intent = new Intent(getActivity(), DetailActivity.class);
            String itemString = new Gson().toJson(currentWeatherItem);
            intent.putExtra(Constants.INTENT_KEY_WEATHER_ITEM, itemString);
            startActivity(intent);
        });

        getCompositeDisposable().add(itemClickedDisposable);
    }

    @Override
    protected void setupOutputs(@NonNull View view) {
        MainActivity mainActivity = (MainActivity) getActivity();
        if (mainActivity != null) {
            mainActivity.getCurrentWeatherItemList().observe(this, list -> {
                citiesAdapter.setItems(list);
            });
        }
    }
}
