package com.indeemand.android.homecreditexam.ui.main;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.jakewharton.rxbinding2.view.RxView;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class CitiesRecyclerViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public PublishSubject<CurrentWeatherItem> itemClicked = PublishSubject.create();

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    private List<CurrentWeatherItem> items = new ArrayList<>();

    public List<CurrentWeatherItem> getItems() {
        return items;
    }

    public void setItems(List<CurrentWeatherItem> items) {
        this.items = items;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(
                R.layout.list_item_city, viewGroup, false);
        return new RecyclerView.ViewHolder(view) {};
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        View view = viewHolder.itemView;
        Context context = viewHolder.itemView.getContext();
        CurrentWeatherItem item = items.get(i);

        Disposable d = RxView.clicks(view).subscribe(
                s -> itemClicked.onNext(item));

        TextView txtName = view.findViewById(R.id.txtName);
        TextView txtTemp = view.findViewById(R.id.txtTemp);
        TextView txtWeatherDesc = view.findViewById(R.id.txtWeatherDesc);

        txtName.setText(item.getName());

        String tempString = context.getString(R.string.temperature_label, item.getMainInfo().getTemp());
        txtTemp.setText(tempString);

        if (item.getWeatherInfoList().size() > 0 ) {
            txtWeatherDesc.setText(item.getWeatherInfoList().get(0).getDescription());
        }

    }

    @Override
    public int getItemCount() {
        return items.size();
    }

}
