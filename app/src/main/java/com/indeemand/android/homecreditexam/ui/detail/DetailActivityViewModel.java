package com.indeemand.android.homecreditexam.ui.detail;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.indeemand.android.homecreditexam.base.BaseViewModel;
import com.indeemand.android.homecreditexam.base.WeatherIconUtils;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.indeemand.android.homecreditexam.models.WeatherInfo;
import com.indeemand.android.homecreditexam.networking.WeatherRepository;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableObserver;

public class DetailActivityViewModel extends BaseViewModel {

    private CurrentWeatherItem weatherItem;

    public void setWeatherItem(CurrentWeatherItem weatherItem) {
        this.weatherItem = weatherItem;
        updateDetails();
    }

    private void updateDetails() {
        cityName.postValue(weatherItem.getName());
        temperatureValue.postValue(weatherItem.getMainInfo().getTemp());

        if (weatherItem.getWeatherInfoList().size() > 0) {
            WeatherInfo first = weatherItem.getWeatherInfoList().get(0);
            weatherDescription.postValue(first.getDescription());
            iconUrlString.postValue(WeatherIconUtils.getUrlStringForIconName(first.getIcon()));
        }

    }

    private MutableLiveData<String> cityName = new MutableLiveData<>();

    public LiveData<String> getCityName() {
        return cityName;
    }

    private MutableLiveData<Float> temperatureValue = new MutableLiveData<>();

    public LiveData<Float> getTemperatureValue() {
        return temperatureValue;
    }

    private MutableLiveData<String> weatherDescription = new MutableLiveData<>();

    public LiveData<String> getWeatherDescription() {
        return weatherDescription;
    }

    private MutableLiveData<String> iconUrlString = new MutableLiveData<>();

    public LiveData<String> getIconUrlString() {
        return iconUrlString;
    }

    private void fetchCurrentWeatherItem() {
        List<String> ids = new ArrayList<>();
        ids.add(String.valueOf(weatherItem.getId()));

        WeatherRepository.getCurrentWeatherByCityId(ids)
                .subscribe(new DisposableObserver<List<CurrentWeatherItem>>() {
                    @Override
                    public void onNext(List<CurrentWeatherItem> currentWeatherItems) {
                        if (currentWeatherItems.size() > 0) {
                            CurrentWeatherItem first = currentWeatherItems.get(0);
                            setWeatherItem(first);
                        }
                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onComplete() {

                    }
                });
    }

    public void refreshButtonClicked() {
        fetchCurrentWeatherItem();
    }
}
