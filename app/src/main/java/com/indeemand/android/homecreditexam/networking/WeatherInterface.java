package com.indeemand.android.homecreditexam.networking;

import com.indeemand.android.homecreditexam.models.CurrentWeatherResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface WeatherInterface {

    @GET("/data/2.5/group")
    Single<CurrentWeatherResponse> getCurrentWeatherByCityId(
            @Query("id") String idString,
            @Query("units") String units,
            @Query("lang") String languageCode,
            @Query("APPID") String appId
    );
}
