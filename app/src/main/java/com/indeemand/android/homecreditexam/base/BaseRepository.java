package com.indeemand.android.homecreditexam.base;

import com.indeemand.android.homecreditexam.BuildConfig;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class BaseRepository<T> {

    public BaseRepository(String baseUrl, Class<T> clazz) {
        this.baseUrl = baseUrl;
        this.clazz = clazz;
    }

    private String baseUrl;
    private Class<T> clazz;

    private T apiInterface;

    protected T getApiInterface() {
        if (apiInterface == null) {
            apiInterface = new retrofit2.Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(createOkHttpClient())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                    .create(clazz);
        }
        return apiInterface;
    }

    private OkHttpClient createOkHttpClient() {
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS);

        if (BuildConfig.enableHttpLogging) {
            builder.addInterceptor(
                    new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY));
        }

        return builder.build();
    }


}
