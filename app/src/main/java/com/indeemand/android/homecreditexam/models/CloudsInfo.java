package com.indeemand.android.homecreditexam.models;

import com.google.gson.annotations.SerializedName;

public class CloudsInfo {

    @SerializedName("all")
    private int all;

    public int getAll() {
        return all;
    }

    public void setAll(int all) {
        this.all = all;
    }
}
