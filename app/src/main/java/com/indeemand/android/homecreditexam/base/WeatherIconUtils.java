package com.indeemand.android.homecreditexam.base;

public class WeatherIconUtils {

    public static String getUrlStringForIconName(String iconName) {
        return "http://openweathermap.org/img/w/" + iconName+ ".png";
    }
}
