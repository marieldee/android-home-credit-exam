package com.indeemand.android.homecreditexam.base;

public class RxVoid {

    private static RxVoid aVoid;

    public static RxVoid getInstance() {
        if (aVoid == null) {
            aVoid = new RxVoid();
        }
        return aVoid;
    }
}
