package com.indeemand.android.homecreditexam.ui.detail;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.base.BaseActivity;
import com.indeemand.android.homecreditexam.base.Constants;
import com.indeemand.android.homecreditexam.base.GlideApp;
import com.indeemand.android.homecreditexam.base.RxVoid;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.disposables.Disposable;

public class DetailActivity extends BaseActivity<DetailActivityViewModel> {

    private TextView txtCityName;
    private TextView txtWeatherDesc;
    private TextView txtTemp;
    private ImageView imgIcon;
    private Button refreshBtn;

    @Override
    protected String getPageName() {
        return "City Weather Details Activity";
    }

    @Override
    protected int getLayoutId() {
        return R.layout.activity_detail;
    }

    @Override
    protected Class<DetailActivityViewModel> getViewModelClass() {
        return DetailActivityViewModel.class;
    }

    @Override
    protected void setupViews() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

        txtCityName = findViewById(R.id.txtName);
        txtTemp = findViewById(R.id.txtTemp);
        txtWeatherDesc = findViewById(R.id.txtWeatherDesc);
        imgIcon = findViewById(R.id.imgIcon);
        refreshBtn = findViewById(R.id.btnRefresh);

    }

    @Override
    protected void setupDependencies() {
        if (getIntent().hasExtra(Constants.INTENT_KEY_WEATHER_ITEM)) {
            String itemString = getIntent().getStringExtra(Constants.INTENT_KEY_WEATHER_ITEM);
            CurrentWeatherItem item = new Gson().fromJson(itemString, CurrentWeatherItem.class);
            getViewModel().setWeatherItem(item);
        }
    }

    @Override
    protected void setupInputs() {
        Disposable d = RxView.clicks(refreshBtn).subscribe(
                s -> getViewModel().refreshButtonClicked());

        getCompositeDisposable().add(d);
    }

    @Override
    protected void setupOutputs() {
        getViewModel().getCityName().observe(this, string -> {
            txtCityName.setText(string);
            setTitle(string);
        });

        getViewModel().getTemperatureValue().observe(this, temp -> {
            String tempString = getString(R.string.temperature_label, temp);
            txtTemp.setText(tempString);
        });

        getViewModel().getWeatherDescription().observe(this, string -> {
            txtWeatherDesc.setText(string);
        });

        getViewModel().getIconUrlString().observe(this, url -> {
            GlideApp.with(this)
                    .load(url)
                    .fitCenter()
                    .dontAnimate()
                    .into(imgIcon);
        });
    }
}
