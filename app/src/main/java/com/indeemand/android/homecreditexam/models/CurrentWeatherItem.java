package com.indeemand.android.homecreditexam.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentWeatherItem {

    @SerializedName("coord")
    private Coordinates coordinates;

    @SerializedName("sys")
    private SysInfo sysInfo;

    @SerializedName("weather")
    private List<WeatherInfo> weatherInfoList;

    @SerializedName("main")
    private MainInfo mainInfo;

    @SerializedName("visibility")
    private int visibility;

    @SerializedName("wind")
    private WindInfo windInfo;

    @SerializedName("clouds")
    private CloudsInfo cloudsInfo;

    @SerializedName("dt")
    private int dt;

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    public Coordinates getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

    public SysInfo getSysInfo() {
        return sysInfo;
    }

    public void setSysInfo(SysInfo sysInfo) {
        this.sysInfo = sysInfo;
    }

    public List<WeatherInfo> getWeatherInfoList() {
        return weatherInfoList;
    }

    public void setWeatherInfoList(List<WeatherInfo> weatherInfoList) {
        this.weatherInfoList = weatherInfoList;
    }

    public MainInfo getMainInfo() {
        return mainInfo;
    }

    public void setMainInfo(MainInfo mainInfo) {
        this.mainInfo = mainInfo;
    }

    public int getVisibility() {
        return visibility;
    }

    public void setVisibility(int visibility) {
        this.visibility = visibility;
    }

    public WindInfo getWindInfo() {
        return windInfo;
    }

    public void setWindInfo(WindInfo windInfo) {
        this.windInfo = windInfo;
    }

    public CloudsInfo getCloudsInfo() {
        return cloudsInfo;
    }

    public void setCloudsInfo(CloudsInfo cloudsInfo) {
        this.cloudsInfo = cloudsInfo;
    }

    public int getDt() {
        return dt;
    }

    public void setDt(int dt) {
        this.dt = dt;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
