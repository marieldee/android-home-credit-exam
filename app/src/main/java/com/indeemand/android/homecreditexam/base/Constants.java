package com.indeemand.android.homecreditexam.base;

public class Constants {
    public static final String OPEN_WEATHER_API_KEY = "76b479387c178940bad8f559ffc42be0";

    public static final String OPEN_WEATHER_SAN_FRANCISCO_ID = "5391997";
    public static final String OPEN_WEATHER_LONDON_ID = "2643743";
    public static final String OPEN_WEATHER_PRAGUE_ID = "3067696";

    public static final String OPEN_WEATHER_UNITS_METRIC = "metric";

    public static final String INTENT_KEY_WEATHER_ITEM = "intent_key_weather_item";
}
