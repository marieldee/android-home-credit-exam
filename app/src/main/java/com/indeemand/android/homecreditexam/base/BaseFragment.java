package com.indeemand.android.homecreditexam.base;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import io.reactivex.disposables.CompositeDisposable;

public abstract class BaseFragment<T extends BaseFragmentViewModel> extends Fragment {

    protected void setupInputs(@NonNull View view) {}
    protected void setupOutputs(@NonNull View view) {}
    protected void setupDependencies() {}
    protected void setupViews(@NonNull View view) {}

    private T viewModel;
    protected T getViewModel() { return viewModel; }

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected CompositeDisposable getCompositeDisposable() { return compositeDisposable; }

    protected abstract String getPageName();
    protected abstract int getLayoutId();
    protected abstract Class<T> getViewModelClass();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        setupDependencies();

        return inflater.inflate(getLayoutId(), container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        viewModel = ViewModelProviders.of(this).get(getViewModelClass());

        setupViews(view);
        setupInputs(view);
        setupOutputs(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
