package com.indeemand.android.homecreditexam.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class CurrentWeatherResponse {

    @SerializedName("cnt")
    private int count;

    @SerializedName("list")
    private List<CurrentWeatherItem> weatherList;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<CurrentWeatherItem> getWeatherList() {
        return weatherList;
    }

    public void setWeatherList(List<CurrentWeatherItem> weatherList) {
        this.weatherList = weatherList;
    }
}
