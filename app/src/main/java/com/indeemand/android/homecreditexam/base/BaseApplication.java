package com.indeemand.android.homecreditexam.base;

import android.app.Application;
import android.content.Context;

import com.google.firebase.FirebaseApp;

public class BaseApplication extends Application {

    private static Context appContext;

    @Override
    public void onCreate() {
        super.onCreate();
        appContext = getApplicationContext();

        FirebaseApp.initializeApp(this);
    }

    public static Context getAppContext() {
        return appContext;
    }
}
