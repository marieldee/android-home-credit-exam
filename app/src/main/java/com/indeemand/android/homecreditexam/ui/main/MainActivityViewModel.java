package com.indeemand.android.homecreditexam.ui.main;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.util.Log;

import com.indeemand.android.homecreditexam.base.BaseViewModel;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.indeemand.android.homecreditexam.networking.WeatherRepository;

import java.util.List;

import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;

public class MainActivityViewModel extends BaseViewModel {

    private MutableLiveData<List<CurrentWeatherItem>> currentWeatherItemList = new MutableLiveData<>();

    public LiveData<List<CurrentWeatherItem>> getCurrentWeatherItemList() {
        return currentWeatherItemList;
    }

    public MainActivityViewModel() {
        fetchCurrentWeatherList();
    }

    private void fetchCurrentWeatherList() {
        WeatherRepository.getCurrentWeatherOfLondonPragueSanFo()
            .subscribe(new DisposableObserver<List<CurrentWeatherItem>>() {
                @Override
                public void onNext(List<CurrentWeatherItem> currentWeatherItems) {
                    currentWeatherItemList.postValue(currentWeatherItems);
                }

                @Override
                public void onError(Throwable e) {

                }

                @Override
                public void onComplete() {

                }
            });
    }

    public void refreshButtonClicked() {
        fetchCurrentWeatherList();
    }
}
