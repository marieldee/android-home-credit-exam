package com.indeemand.android.homecreditexam.ui.main;

import android.arch.lifecycle.LiveData;

import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.base.BaseActivity;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;

import java.sql.Ref;
import java.util.List;

import io.reactivex.disposables.Disposable;

/**
 * @author Mariel Dee
 * @since 09/24/2018
 */
public class MainActivity extends BaseActivity<MainActivityViewModel> {

    private RefreshFragment refreshFragment;
    private CitiesFragment citiesFragment;

    @Override
    protected int getLayoutId() {
        return R.layout.activity_main;
    }

    @Override
    protected String getPageName() {
        return "Main Activity";
    }

    @Override
    protected Class<MainActivityViewModel> getViewModelClass() { return MainActivityViewModel.class; }

    @Override
    protected void setupViews() {
        refreshFragment = (RefreshFragment) getSupportFragmentManager().findFragmentById(R.id.frgRefresh);
        citiesFragment = (CitiesFragment) getSupportFragmentManager().findFragmentById(R.id.frgList);
    }

    @Override
    protected void setupInputs() {
        Disposable refreshDisposable = refreshFragment.refreshClicked
                .subscribe(aVoid -> getViewModel().refreshButtonClicked());

        getCompositeDisposable().add(refreshDisposable);
    }

    public LiveData<List<CurrentWeatherItem>> getCurrentWeatherItemList() {
        return getViewModel().getCurrentWeatherItemList();
    }
}
