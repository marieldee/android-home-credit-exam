package com.indeemand.android.homecreditexam.ui.main;

import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;

import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.base.BaseFragment;
import com.indeemand.android.homecreditexam.base.RxVoid;
import com.jakewharton.rxbinding2.view.RxView;

import io.reactivex.disposables.Disposable;
import io.reactivex.subjects.PublishSubject;

public class RefreshFragment extends BaseFragment<RefreshFragmentViewModel> {

    public PublishSubject<RxVoid> refreshClicked = PublishSubject.create();

    private Button refreshBtn;

    @Override
    protected String getPageName() {
        return "Refresh Button View";
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_refresh_button;
    }

    @Override
    protected Class<RefreshFragmentViewModel> getViewModelClass() {
        return RefreshFragmentViewModel.class;
    }

    @Override
    protected void setupViews(@NonNull View view) {
        refreshBtn = view.findViewById(R.id.btnRefresh);
    }

    @Override
    protected void setupInputs(@NonNull View view) {
        Disposable d = RxView.clicks(refreshBtn).subscribe(
                s -> refreshClicked.onNext(RxVoid.getInstance()));

        getCompositeDisposable().add(d);
    }
}
