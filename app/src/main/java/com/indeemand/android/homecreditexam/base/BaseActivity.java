package com.indeemand.android.homecreditexam.base;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.google.firebase.analytics.FirebaseAnalytics;

import io.reactivex.disposables.CompositeDisposable;

/**
 * @author Mariel Dee
 * @since 09/24/2018
 */
public abstract class BaseActivity<T extends BaseViewModel> extends AppCompatActivity {

    private FirebaseAnalytics firebaseAnalytics;

    protected void setupInputs() {}
    protected void setupOutputs() {}
    protected void setupDependencies() {}
    protected void setupViews() {}

    private T viewModel;
    protected T getViewModel() { return viewModel; }

    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    protected CompositeDisposable getCompositeDisposable() { return compositeDisposable; }

    protected abstract String getPageName();
    protected abstract int getLayoutId();
    protected abstract Class<T> getViewModelClass();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        firebaseAnalytics = FirebaseAnalytics.getInstance(this);

        firebaseAnalytics.setCurrentScreen(this, getPageName(), null);

        viewModel = ViewModelProviders.of(this).get(getViewModelClass());

        setupDependencies();

        setContentView(getLayoutId());

        setupViews();
        setupInputs();
        setupOutputs();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        compositeDisposable.dispose();
    }
}
