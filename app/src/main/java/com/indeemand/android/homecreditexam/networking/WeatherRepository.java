package com.indeemand.android.homecreditexam.networking;

import android.util.Log;

import com.indeemand.android.homecreditexam.R;
import com.indeemand.android.homecreditexam.base.BaseApplication;
import com.indeemand.android.homecreditexam.base.BaseRepository;
import com.indeemand.android.homecreditexam.base.Constants;
import com.indeemand.android.homecreditexam.models.CurrentWeatherItem;
import com.indeemand.android.homecreditexam.models.CurrentWeatherResponse;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.AsyncSubject;

public class WeatherRepository extends BaseRepository<WeatherInterface> {

    private static WeatherRepository INSTANCE;
    private static String TAG = "WeatherRepository";

    private WeatherRepository() {
        super("http://api.openweathermap.org", WeatherInterface.class);
    }

    private static WeatherRepository getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new WeatherRepository();
        }
        return INSTANCE;
    }

    public static AsyncSubject<List<CurrentWeatherItem>> getCurrentWeatherByCityId(
            List<String> cityIds) {

        final AsyncSubject<List<CurrentWeatherItem>> asyncSubject = AsyncSubject.create();

        String idString = android.text.TextUtils.join(",", cityIds);

        getInstance().getApiInterface()
                .getCurrentWeatherByCityId(
                        idString,
                        Constants.OPEN_WEATHER_UNITS_METRIC,
                        null,
                        Constants.OPEN_WEATHER_API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new SingleObserver<CurrentWeatherResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(CurrentWeatherResponse currentWeatherResponse) {
                        asyncSubject.onNext(currentWeatherResponse.getWeatherList());
                        asyncSubject.onComplete();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Log.d("TAG", "getCurrentWeatherByCityId error: " + e.getLocalizedMessage());
                        String errorMessage = BaseApplication.getAppContext()
                                .getString(R.string.api_generic_error);

                        asyncSubject.onError(new Throwable(errorMessage));

                    }
                });


        return asyncSubject;
    }

    public static AsyncSubject<List<CurrentWeatherItem>> getCurrentWeatherOfLondonPragueSanFo() {
        List<String> idList = new ArrayList<>();
        idList.add(Constants.OPEN_WEATHER_LONDON_ID);
        idList.add(Constants.OPEN_WEATHER_PRAGUE_ID);
        idList.add(Constants.OPEN_WEATHER_SAN_FRANCISCO_ID);

        return getCurrentWeatherByCityId(idList);
    }
}
